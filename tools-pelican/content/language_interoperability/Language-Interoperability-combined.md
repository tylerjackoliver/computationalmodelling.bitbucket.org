title: Language Interoperability
date: 2019-02-19
modified: 2019-02-19
category: Workshops
tags: Fortran, Python, Cython, C
slug: Language-Interoperability
authors: Rebecca Clements, Jack Tyler
summary: A guide to language interoperability in HPC

<img src="/language_interoperability/header_image.png" alt="Header" width="750"/>

<a href="language_interoperability.pdf">Workshop slides</a>


Developers working on large software projects rarely manage to find just one language to meet all of their requirements, or at least at their desired performance. Inter-disciplinary projects, particularly in academia, often use multiple in-house softwares written in different languages. Language interoperability - the ability to call a function written in one language from another - is not only useful for collaborations between language users, but also for enabling the migration of code to a different language, including making use of old code.

Interoperability allows the designer to combine the best of several languages. Indeed, one may prefer the readable, simple syntax of Python, but also desire the speed of the lower-level, compiled C. For example, running the attached algorithm for the Fibonacci sequence in Python ([```.py file```](fib.py): ~30 seconds) is much slower than running in C ([```.c file```](fib.c): ~0.02 seconds). Alternatively, an efficient implementation of a numerical method may already exist in a Fortran library, hence having the ability to call this library in your language of choice could save considerable time and effort.

This blog post will introduce the concept of interoperability by demonstrating how to combine the high-level Python language with the speed of C, using Cython. Focus will then turn to an application more suited to HPC, by studying how Fortran subroutines can be called from C.

## How low can you go? Calling C from Python with Cython

Cython is a stand-alone programming language written to be a superset of the Python language. Written mostly in Python, it is designed to give optimised performance similar to C. This is achieved by translating existing Python code into C, which may then be imported using standard Python ```import``` syntax to provide generally faster results. To fully take advantage of Cython, an additional feature is its foreign function interface (FFI) for invoking existing C functions wrapped into a Cython module.

The FFI included in Cython is the focus of the following section, however, it is worthwhile to first study the general syntax and workflow of using Cython.

### The Cython Workflow

Cython can be installed readily via Pip:

```pip install cython```

While Cython's syntax is largely similar to Python, there are a few subtle differences: Cython files have the extension ```.pyx```, and to incorporate C, C-style declarations can be used and compilation is required. It's best to demonstrate with an example; consider the following pure Python code, computing the Fibonacci sequence:

```{python}
# fib.py:

def fib(n):

	"""Compute the n-th Fibonacci number"""
	
	if n < 2:
	
		return n
		
	else:
	
		return fib(n-1) + fib(n-2)

```

Calling this function is as simple as executing the following at a Python prompt:

```{python}
>> from fib import fib
>> fib(3)
6
```

as is typical for Python routines. This program is easily translated over to a naïve Cython implementation, in an equivalent ```fib.pyx``` file, by simply changing the line

```{cython}
def fib(int n):
```

Note the inclusion of a type specifier for ```n```. In Cython, the contents of the ```.pyx``` is converted to (close to) pure C, and as a result we have access to C-style function declarations. Our code will compile without static-type declarations, but we can include them to let the compiler know the type in advance, and speed up our program execution all the more.

In order to make this file importable into Python, it must be *cythonized*; that is, the Cython compiler must be run to produce a shared object file (```.so```), that can be imported into Python using the standard import syntax, as above. An object file (```.o```) stores the compiled code and data for all functions involved, in binary form. This is generally not directly executable and is for use in creating the full program, where it is combined with other object files. A shared object file (```.so```) is simply a combination (done via 'linking') of all the object files required for execution; this holds all compiled information and can be directly executable.

We can run the ```cythonize``` command included with Cython to produce the ```fib.so``` file with the ```-i``` argument to build in-place.

```
cythonize -i fib.pyx
```

This file can then be imported and called in the same way as a standard Python module.

```
>> from fib import fib
>> fib(3)
6
```

To demonstrate the speed up, in the Python console, we will first time how long it takes to compute the 10th Fibonacci number. We will repeat this 1,000,000 times for a runtime of comparable magnitude.

```
>>> from timeit import timeit
>>> mysetup = "from fib import fib"
>>> mycode = '''fib(10)'''
>>> print(timeit(setup = mysetup, stmt = mycode, number = 1000000))
29.905888451001374
```

This takes 29.91 seconds. By doing the same with the Cython file, ```fib.pyx```, above with one static-type declaration, we can see that this takes considerably less time (5.26 seconds), even for this small example. Note - take care with identical Python and Cython file names in the Python console.

```
5.262989983981242
```

The implementation above - a direct copy of the pure Python code - is described by Cython as naïve because it takes no advantage of the Cython architecture. More information on the different ranges of functions and variable declarations available in Cython is readily available in the [Cython documentation](https://cython.readthedocs.io/en/latest/index.html).

### What do you mean I can't ```from c import *```?

Cython's FFI makes it incredibly easy to interface existing C libraries into Python, using the ```cdef extern from``` syntax. 

For example, consider we wish to borrow the ```sin``` function from C's Math library. A quick ```man sin``` at the command line will provide us the function prototype for the C command included as part of a standard *nix install in `math.h`:

```{C}
double sin(double x);
```

Importing this into Python is made relatively painless using Cython. By declaring the `sin` function as a function provided by the `math` library in C, we allow ourselves to use the [cdef extern from](https://cython.readthedocs.io/en/latest/src/userguide/external_C_code.html) syntax. However, by default, Python can't read Cython, and thus we must always provide a Python-compatible stub function, to interface our Cython and Python.

```
# call_c.pyx:

# We want to use a function from "math.h"...

cdef extern from "math.h":

	# And now provide the function prototype to Cython

	double sin(double x)
	
# Unfortunately Python can't see the above, but
# Cython can create a Python function to translate
# for us:

def call_sin(x):
	"""
	
	Stub wrapping function to allow calling the sin()
	function made available by the math.h library.
	
	"""

	return sin(x)

```

Calling the `sin` function from the `math` library is then as simple as importing the `call_c` shared object file generated when Cythonizing the `call_c.pyx` file, as before:

```
>> from call_c import *
>> call_sin(0.)
0.
```

## C/Fortran Interoperability

### Introduction to Fortran

Formula Translation, or Fortran, was developed in 1957 as a general-purpose programming language for the IBM704 computing system, but has found modern use for numerical and scientific computing applications. Fortran is the dominant language in use on the ARCHER supercomputing cluster (Figure 1.)

---
<img src="/language_interoperability/languages.png" alt="Header" width="500"/>

Figure 1: Node hours per language used in January 2019 on the ARCHER supercomputing cluster.

---


The language has undergone major revisions since its introduction, with each defining a new standard: 

* FORTRAN was the earliest iteration; followed by FORTRAN II through IV, FORTRAN 66 and 77.

* Then there was a major specification update: Fortran 90, known as 'Modern Fortran' due to a changed stylisation.

* Fortran 2003 and 2018 are the latest, with minor revisions in 1995 and 2008.

Most importantly, the 2003 specification formally made interoperating Fortran and C codes part of the Fortran standard, with more functionality in 2018.

### Basic Fortran Syntax

Fortran, unlike its distant relative C, was designed for use by a non-expert application developer; those with little knowledge of computer science concepts such as machine addresses. Fortran allows the programmer to focus on the problem at hand rather than the small, low-level details, making it easy to get a job up-and-running.

The following basics are a good starting point but for further material refer to [an existing introductory workshop](https://computationalmodelling.bitbucket.io/tools/FORTRAN.html).

We will first present a sample function to show the basic syntax, where ```!``` denotes comments:


```{fortran}
program main					  	    ! Start the program called main

	implicit none 					    ! Disable implicit variable typing
	
	integer :: a = 4
	integer :: i
	real    :: b = 4.0				    ! If no kind is supplied, it defaults to 4-bytes (single precision)
	real 	:: c = 4.0
	real    :: d
	character(len=23) :: str
	
	str = "a is 4 and b is 4.0" 	    ! Assign to string
	
	d = b * c						    ! Basic arithmetic
	
	call some_subroutine(a, b, c)	    ! Call subroutine
	
	write(*,*) str					    ! Write str to stdout
	
	do i = 1,10						    ! For loop: for (i=1; i<10; i++)
	
		! some stuff
		
	end do
	
	if (d < 5) then					    ! If statement
	
		write(*,*) "d is less than 5!"
		
	else if (d > 5) then			    ! else if
	
		write(*,*) "d is greater than 5!"
	
	else
	
		write(*,*) "d is equal to 5!"
		
	end if
	
end program main					    ! End main
	
```

* A *program* contains statements and *subprograms* that perform a function. The core source file must include a  `main` program.

* A *subprogram* is either a *subroutine*, with multiple input arguments and outputs allowed, or a *function* which returns one value but may take any number of input arguments.

* Arithmetic statements use the typical operators: *, +, -, /, =.

* Fortran, like C, is statically-typed, so variables must be declared with a type before use. By default, variables have a type depending on the first character of their name. To disable this (strongly recommended), start your Fortran program wth `implicit none`.

* Import external *libraries* or *modules* with `use`. For example, `use iso_c_binding` loads the module defining constants for use with the `kind` parameter in C/Fortran interlinking (see below).

* Control and repetition statements are similar to Python declarations, but are terminated with `end <statement-type>`, (as shown in the example above):
	- `end do` ends repetition structures, `do` (similar to `for` loops), and control structures, `while`
	-  `end if` ends `if` statements, coupled with logical operators `> >= < <= ==` and statements `.and., .not.`,

* The file suffix `.F` is used for older formats (pre-Fortran 90). `.fxx` is now used, where xx is the specification year. In this workshop, we will use `.f03` files for C interoperability in the 2003 specification.

Fortran supports multiple variable types, and almost all can be directly compared with a C equivalent.

| C Variable Type | Fortran Variable Type  | Fortran example | Description |
| :-------------- | :-------------------- | :---------------| :---------- |
| ```int```       | ```integer```		       | ```integer :: a```| Initialise `a` to be type integer
| ```double```    | ```real```| ```real(kind=REAL64) :: a``` | Initialise `a` to be a 64-bit real or use a different `kind` |
| ```char```		 | ```character```	      | `character :: a` | Initialise `a` to a single character |
| ```char s[4]``` | ```character(len=4)``` | ```character(len=4) :: a``` | Assign a 4-long character array to `a`|
| ```double s[x][y]``` | `real, dimension(x,y)` | `real, dimension(x,y) :: a` | Create a 4x4 array of reals|



Fortran's subroutine and function syntax, however, is quite different to that of C. A subroutine is defined as thus:

```
subroutine my_subroutine(input, output1, output2)
	
	variable_type, intent(in) :: input
	variable_type, intent(out) :: output1
	variable_type, intent(out) :: output2

	...

end subroutine my_subroutine
```

where the `intent` attribute is described in [compilation](#compilation). The function syntax is like so:

```
function my_function(input) returns(output)

	...

end function my_function

```

### Compiling via Object files

Inter-operating C and Fortran code requires combining or *linking* separate compiled codes in C and Fortran into a single executable. Knowing how an executable comes together is useful for understanding how to interoperate C and Fortran.

The compilation of a generic program involves three steps:

>* Preprocessing: source code -> 'pure' source code
>
>* Compilation: pure source code -> object file.
>
>* Linking: object files -> executable (or shared library file)

#### Preprocessing

The preprocessor works with the *preprocessor directives*, defined by the leading ```#``` character in the source code, like `#include` and `#define`. It produces 'pure' source code, with no processor directives, by replacing ```#include``` with the content of the respective file, `#define` macros with their values, and any `#if`, `#ifdef` and `#ifndef` with corresponding sections of code.

#### Compilation

The compiler reads the pure source code from the pre-processor, and converts it into assembly code in the form of an object file. This contains metadata about the addresses of its variables and functions, known as symbols. Object files can *refer* to undefined symbols, such as declarations, without explicitly defining them. This is done for interoperating with C and Fortran. The `-c` flag in the GNU compilers stops the compilation at the point of object file production, allowing for the *linking* stage.

#### Linking

We now have multiple object files containing undefined references to other files. The linker combines these into a final executable, piecing together all the undefined symbols, replacing their references with the correct addresses. Optionally, we can store all of these into a special archive called a static library, to easily re-use later on.

#### The C/Fortran Compilation Workflow

<a name="workflow"></a> 
In our case, we wish to link together object files written in C and Fortran to create an executable which uses source code from both languages (Figure 2). We'll be using the standard GNU compilers in this workshop - `gfortran` and `gcc`.

---
<img src="/language_interoperability/c_fortran_objects.png" alt="Linking workflow" width="750"/>

Figure 2: The C/Fortran compilation and linking workflow; notice that we link together individual object files from Fortran and C code to create a single exectuable.

---
<div style="text-align: right"> <a href="#passing">(return to Fortran introduction)</a></div>

Suppose we have an example Fortran subroutine in a module `example_fortran.f03`, and a C function that calls the Fortran subroutine in `example_c.c`; we can compile object files for each of these using their respective compiler.

```
gcc -c example_c.c
gfortran -c example_fortran.f03
```

which produces the output files `example_c.o` and `example_fortran.o`. 

Link these together to create the full executable using either `gfortran` or `gcc`. For consistency we'll use `gcc` in this workshop.

```
gcc example_c.o example_fortran.o -o main.out
```

If we're using additional C or Fortran libraries, we may also need [additional command line arguments](https://www.rapidtables.com/code/linux/gcc/gcc-l.html) when linking.

### Interoperability

<a name="interoperability"></a>

Moving on to C/Fortran interoperabilty, compilers that support the standard provide the module `iso_c_binding` for Fortran. This module provides `kind` specifiers that allow Fortran to match its data-types with C, such as below:

```
program main

	use iso_c_binding			! Fortran is case-insensitive, so upper or lower is fine
	
	implicit none
	
	integer(kind=c_int) :: a.   ! Initialise an integer for use with C
	real(kind=c_double) :: b.   ! Initialise a double for use with C
	
	.
	.
	.
	
end program main
```
More information on other ```kind``` types supported in `iso_c_binding` can be found in the [documentation](http://fortranwiki.org/fortran/show/iso_c_binding).

After declaring the types, we must ensure to include the `bind` attribute from `iso_c_binding` for the Fortran subroutines and functions to be accessible in C. In the following, we `bind c` to the function with `name="pass_by_value"`. Declaring `a, b, c, d` with an `intent(inout)` specifier marks these variables as inputs *and* outputs. The use of `intent(in)` and `intent(out)` specifiers individually is self-explanatory.

```
! Wrapper routine to demonstrate passing by value

subroutine pass_by_value(a, b, c, d) bind(c, name="pass_by_value")                  

    ! Value identifier
    real(kind=c_double), value, intent(inout) :: a, b, c, d  

end subroutine pass_by_value
```

To call this Fortran subroutine in C simply introduce it as an external function with keyword `extern` in the prototype,

```extern int pass_by_value(double a, double b, double c, double d);``` 

before calling, like so:

```
#include <stdio.h>

extern int pass_by_value(double a, double b, double c, double d);   /* Function prototype */

int main(){

	double a = 1, b = 2, c = 3, 

	my_subroutine_in_c(1);
	
	return 0;
	
}

```
<a name="passing"></a>
Note that all Fortran subroutines return type `int` in C, by default. To finish off, we compile our project as described in [the C/Fortran Compiler Workflow](#workflow) earlier.

#### Passing by value and passing by reference

Notice that in our previous example we used the `value` identifier for declaring the variables. This informs Fortran that we will pass our variable to it by its value, which is the default for C unlike *by reference* in Fortran.

If we omit this identifier in the case of variable `c`,

```
subroutine my_subroutine bind(c, name="my_subroutine_in_c")

	integer, intent(in) :: c

	write(*,*) c
	
end subroutine my_subroutine

```
then we must pass *by reference* in our C code, so that the two languages match. We therefore edit our function declaration with a pointer ```*``` and call with an address operator ```&``` accordingly.

```
#include <stdio.h>

extern int my_subroutine_in_c(double *c);

int main(){

	double c = 1;

	my_subroutine_in_c(&c);
	
	return 0;
	
}

```

### Further subtleties: Memory allocation

While Fortran and C are similar languages in their syntax, they handle their memory allocation for multidimensional arrays differently.

> Fortran:
> 
> * The entire array is defined contiguously in memory at the time of memory allocation.
> 
> * Index from 1.

> C:
> 
> * Requires the definition of each of the dimensions of the array separately, and thus there is no guarantee that the array dimensions lie contiguously in memory.
>
> * Index from 0.

Given the differences, using arrays involves translating them into a form compatible with both languages, i.e. making the memory addresses contiguous. We must flatten all of our arrays in C into a single row or column, pass them into Fortran, and then restore their shape afterwards, or vice versa.

Consider a 3x3 array of doubles, `in_array`. To pass this from C to Fortran: first, allocate memory for the flattened array in C and populate it.

```
double * make_array(double in_array[][], int size_array){

	double *p = malloc(size_array * size_array * sizeof(double));
	
	/* Convert to 'flattened' array */
	
	for (int i=0; i < size_array; i++){
	
		for (int j=0; j < size_array; j++){
		
			p[i*size_array+j] = in_array[i][j];		/* Store in row-major */
			
		}
		
	}
	
	return p;
	
}
	
```

Next, we handle the subroutine in Fortran:

```
subroutine fortran_array(a, n) bind(c, name="fortran_array")

	integer(kind=c_int), intent(in) :: n                 	! Size of array (n=3 here)
	
	real(kind=c_double), dimension(:), intent(in) :: a   	! dimension(:) -> implicit array sizing
	
	real(kind=c_double), dimension(:,:), allocatable :: b 	! allocatable -> we'll define its size later, only need its dimension for now
	
	integer :: i, j, k 										! Loop variables
	
	! Restore the size of our array
	
	allocate(b(n, n))										! Allocate memory for b: make it n x n
	
	do i=1,n												! Fortran arrays start at 0
	
		do j = 1,n
	
			b(i,j) = a((i-1)*n+j)							! Note the array indexing syntax
			
		end do
	
	end do
	
	! Print what we received to the screen
	
	do i=1,n
	
		do j=1,n
			
			write(*,*) b(i,j)								! Write to stdout
			
		end do
		
	end do
	
	deallocate(b) 											! Free the pointer; same as free(b) in C
	
end subroutine fortran_array
	
```

Add the function prototype into C:

```
extern int fortran_array(double *a, int n);
```

And bring it all together in C:

```
extern int fortran_array(double *a, int n);

int main(void){

	double in_array[3][3] = {1., 2., 3., 4., 5., 6., 7., 8., 9.};
	int n = 3;
	
	double *p = make_array(&in_array, n);
	
	fortran_array(p, &n);
	
	return 0;
	
}
```

We may extend these basic techniques to interface generally any Fortran library function with a C program via a Fortran wrapping function.

## Conclusion

Language interoperability is a powerful paradigm for constructing faster codes, or for speeding development time. The preceding blog post has outlined the basics of interoperating Python and C, and C and Fortan. Far more literature, however, exists on the topic, and the pitfalls inherent with the method should always be considered when designing-in interoperability.
