#include <stdio.h>
#include <stdlib.h>
#include <time.h>
clock_t startm, stopm;
#define START if ( (startm = clock()) == -1) {printf("Error calling clock");exit(1);} 
#define STOP if ( (stopm = clock()) == -1) {printf("Error calling clock");exit(1);} 
#define PRINTTIME printf( "%8.5f seconds used.\n", (((double) stopm- startm)/CLOCKS_PER_SEC));
#define num 10
 
int main()
{
  int n = num, i;
 
  START;

  for (int repeat=0; repeat<1000000; ++repeat) {
    for (i = 0; i < n; i++)
    {
        int fib(int i);
        /*printf("%d\n", fib(i));*/
    }
  }

  STOP;
  PRINTTIME;
 
  return 0;
}

int fib(int i){
    if (i < 2){
                return i;
    }
            else
            {
                return fib(i-1) + fib(i-2);
            }
}