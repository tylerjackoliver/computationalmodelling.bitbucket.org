"""Task 9: Find and Draw contours with color blue, thickness 3"""
import cv2 as cv

"""Read an image"""
img = cv.imread('logo.png')

"""Get binary image using cv.cvtColor() and cv.inRange()
Use lower_boundary = 170 and upper_boundary = 255 in cv.inRange
"""
imgray = cv.cvtColor(img,cv.COLOR_BGR2GRAY) 
img_binary = cv.inRange(imgray,170,255)
im2, contours, hierarchy = cv.findContours(img_binary,cv.RETR_TREE,cv.CHAIN_APPROX_NONE)


"""Draw all contours with color blue (255,0,0), thickness 3 """
cv.drawContours(img,contours,-1,(255,0,0),3)

"""Display"""
cv.imshow('img',img)
cv.waitKey(0)
cv.destroyAllWindows()
