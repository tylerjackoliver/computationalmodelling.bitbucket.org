SELECT Houses_Table.House, COUNT(Characters_Table.Character), Houses_Table.Words
FROM Characters_Table
JOIN Houses_Table ON Characters_Table.Surname = Houses_Table.House
GROUP BY Houses_Table.House
