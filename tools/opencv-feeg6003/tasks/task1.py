# TASK1 - Managing Images
#Load opencv library
import cv2 
import numpy as nump

#1-Load an color image. The filename provided is: 'atlantis.jpg'.
colorimg = cv2.

#2-Load an image in grayscale mode. The filename is the same as in 
# the previous task.
grayimg = cv2.

#3-Display the color image using the specific function for it.
cv2.

#4-Display the grayscale image aforeloaded.
cv2.

#5-Save the grayscale image into a file named: 'grayimage.jpg'.
cv2.

# Extra code provided
# Wait for a key stroke
cv2.waitKey(0)
# After the key stroke destroy all windows
cv2.destroyAllWindows()
