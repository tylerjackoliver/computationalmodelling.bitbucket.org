Title: Continuous Integration - Jenkins
Date: 2017-09-07 14:00
Category: Workshop
Tags: Continuous Integration, Jenkins
Authors: Peter Bartram, Samuel Diserens

On Thursday 4th May [Peter Bartram](http://cmg.soton.ac.uk/people/pb1n16/) and [Samuel Diserens](http://cmg.soton.ac.uk/people/sdd1n15/) presented a workshop on the practice of Continuous Integration and the use of the open-source software Jenkins <link>. This workshop aimed to introduce participants to some of the key concepts of continuous integration

